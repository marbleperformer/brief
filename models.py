from django.db import models

from modelcluster.fields import ParentalKey

from wagtail.wagtailadmin.edit_handlers import FieldPanel, FieldRowPanel, InlinePanel, MultiFieldPanel

from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailforms.models import AbstractFormField, AbstractEmailForm

class FormField(AbstractFormField):
	page = ParentalKey('BriefPage', related_name='form_fields')

class BriefPage(AbstractEmailForm):
	body = RichTextField(verbose_name='Содержимое')

	content_panels = AbstractEmailForm.content_panels + [
		FieldPanel('body'),
		InlinePanel('form_fields', label='Поля формы'),
		MultiFieldPanel([
			FieldRowPanel([
				FieldPanel('from_address', classname='col6'),
				FieldPanel('to_address', classname='col6'),
			]),
			FieldPanel('subject'),
		], 'Рассылка')
	]

	@classmethod
	def can_create_at(cls, parent):
		return super(BriefPage, cls).can_create_at(parent) and not cls.objects.exists()

	parent_page_types = [
		'landing.LandingPage',
	]

	class Meta:
		verbose_name = 'Бриф страница'
		verbose_name_plural = 'Бриф страница'

